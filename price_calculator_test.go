package shipments

import "testing"

func TestCalculatePrice(t *testing.T) {
	type args struct {
		weightKg    int64
		fromCountry string
	}
	tests := []struct {
		name    string
		args    args
		want    int64
		wantErr bool
	}{
		{
			name: "nordic small",
			args: args{weightKg: 9, fromCountry: "SE"},
			want: 100,
		},
		{
			name: "nordic medium",
			args: args{weightKg: 15, fromCountry: "SE"},
			want: 300,
		},
		{
			name: "nordic large",
			args: args{weightKg: 40, fromCountry: "SE"},
			want: 500,
		},
		{
			name: "nordic huge",
			args: args{weightKg: 500, fromCountry: "SE"},
			want: 2000,
		},
		{
			name: "eu large",
			args: args{weightKg: 42, fromCountry: "AT"},
			want: 750,
		},
		{
			name: "outside eu large",
			args: args{weightKg: 45, fromCountry: "US"},
			want: 1250,
		},
		{
			name:    "out of range",
			args:    args{weightKg: 1001, fromCountry: "US"},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := CalculatePrice(tt.args.weightKg, tt.args.fromCountry)
			if (err != nil) != tt.wantErr {
				t.Errorf("CalculatePrice() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("CalculatePrice() = %v, want %v", got, tt.want)
			}
		})
	}
}
