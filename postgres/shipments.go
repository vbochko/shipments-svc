package postgres

import (
	"database/sql"
	"fmt"
	"time"

	"github.com/Masterminds/squirrel"
	"github.com/jmoiron/sqlx"
	"gitlab.com/vbochko/shipments-svc"
)

// ShipmentsStorage.
type ShipmentsStorage struct {
	db *sqlx.DB
}

// New ShipmentsStorage.
func New(db *sqlx.DB) *ShipmentsStorage {
	return &ShipmentsStorage{db: db}
}

// Add new Shipment, db generated id is set to argument.
func (s *ShipmentsStorage) Add(shipment *shipments.Shipment) error {
	tx, err := s.db.Beginx()
	if err != nil {
		return fmt.Errorf("failed to begin tx: %w", err)
	}
	defer func() { _ = tx.Rollback() }()

	if err := s.insertContact(tx, &shipment.From); err != nil {
		return fmt.Errorf("failed to insert 'from' contact: %w", err)
	}

	if err := s.insertContact(tx, &shipment.To); err != nil {
		return fmt.Errorf("failed to insert 'to' contact: %w", err)
	}

	shipment.CreatedAt = time.Now().UTC() // reset just in case
	query := squirrel.Insert("shipments").SetMap(map[string]interface{}{
		"from_id":    shipment.From.ID,
		"to_id":      shipment.To.ID,
		"weight":     shipment.WeightKg,
		"price":      shipment.PriceSEK,
		"created_at": shipment.CreatedAt,
	}).Suffix("RETURNING id")

	if err := s.get(tx, &shipment.ID, query); err != nil {
		return fmt.Errorf("failed to insert shipment: %w", err)
	}

	return tx.Commit()
}

// Get Shipment by id.
func (s *ShipmentsStorage) Get(id int64) (*shipments.Shipment, error) {
	query := buildSelectShipmentsQuery().
		Where("shipments.id = ?", id)

	rawSql, args, _ := query.ToSql()
	row := s.db.QueryRowx(s.db.Rebind(rawSql), args...)

	var shipment shipments.Shipment
	switch err := scanShipment(&shipment, row); err {
	case sql.ErrNoRows:
		return nil, nil
	case nil:
		return &shipment, nil
	default:
		return nil, err
	}
}

// List shipments.
func (s *ShipmentsStorage) List(params *shipments.ListShipmentsParams) ([]shipments.Shipment, error) {
	query := buildSelectShipmentsQuery().
		OrderBy("shipments.id ASC").
		Limit(params.Limit).
		Offset(params.Page * params.Limit)

	rawSql, args, _ := query.ToSql()
	rows, err := s.db.Queryx(s.db.Rebind(rawSql), args...)
	if err != nil {
		return nil, fmt.Errorf("failed to get list of shipments: %w", err)
	}

	var result []shipments.Shipment
	for rows.Next() {
		var dest shipments.Shipment
		if err := scanShipment(&dest, rows); err != nil {
			return nil, fmt.Errorf("failed to scan shipment: %w", err)
		}

		result = append(result, dest)
	}

	return result, nil
}

func scanShipment(dest *shipments.Shipment, scanner sqlx.ColScanner) error {
	return scanner.Scan(
		&dest.ID,
		&dest.PriceSEK,
		&dest.WeightKg,
		&dest.CreatedAt,
		&dest.From.ID,
		&dest.From.Name,
		&dest.From.Email,
		&dest.From.Address,
		&dest.From.CountryCode,
		&dest.To.ID,
		&dest.To.Name,
		&dest.To.Email,
		&dest.To.Address,
		&dest.To.CountryCode,
	)
}

func (s *ShipmentsStorage) insertContact(queryer sqlx.Queryer, contact *shipments.Contact) error {
	query := squirrel.Insert("contacts").SetMap(map[string]interface{}{
		"name":         contact.Name,
		"email":        contact.Email,
		"address":      contact.Address,
		"country_code": contact.CountryCode,
	}).Suffix("RETURNING id")

	return s.get(queryer, &contact.ID, query)
}

func (s *ShipmentsStorage) get(queryer sqlx.Queryer, dest interface{}, query squirrel.Sqlizer) error {
	rawSql, args, err := query.ToSql()
	if err != nil {
		return fmt.Errorf("invalid query: %w", err)
	}

	return sqlx.Get(queryer, dest, s.db.Rebind(rawSql), args...)
}

func buildSelectShipmentsQuery() squirrel.SelectBuilder {
	return squirrel.Select(
		"shipments.id", "shipments.price", "shipments.weight", "shipments.created_at",
		"senders.id", "senders.name", "senders.email", "senders.address", "senders.country_code",
		"receivers.id", "receivers.name", "receivers.email", "receivers.address", "receivers.country_code",
	).From("shipments").
		Join("contacts AS senders ON shipments.from_id = senders.id").
		Join("contacts AS receivers ON shipments.to_id = receivers.id")
}
