package postgres

import (
	"os"
	"testing"
	"time"

	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
	migrate "github.com/rubenv/sql-migrate"
	"gitlab.com/vbochko/shipments-svc"
	"gitlab.com/vbochko/shipments-svc/assets"
)

func TestShipmentsStorage_Add(t *testing.T) {
	dsn, ok := os.LookupEnv("TESTDB")
	if !ok {
		t.Skip("please set env var TESTDB in order to run tests against db")
	}

	db := sqlx.MustConnect("postgres", dsn)
	if err := setup(db); err != nil {
		t.Fatalf("failed to setup testdb: %v", err)
	}
	defer func() { _ = teardown(db) }()

	shipment := shipments.Shipment{
		From: shipments.Contact{
			Name:        "Tester Sender",
			Email:       "tester-sender@gmail.com",
			Address:     "1 Sumska Str, Kharkiv",
			CountryCode: "UA",
		},
		To: shipments.Contact{
			Name:        "Tester Receiver",
			Email:       "tester-receiver@gmail.com",
			Address:     "1 Derybasivska Str, Odesa",
			CountryCode: "UA",
		},
		WeightKg: 10,
		PriceSEK: 100,
	}

	storage := New(db)
	if err := storage.Add(&shipment); err != nil {
		t.Fatalf("failed to add new shipment %v", err)
	}

	if shipment.ID == 0 {
		t.Fatal("auto-generated shipment id was not set")
	}

	actual, err := storage.Get(shipment.ID)
	if err != nil {
		t.Fatalf("failed to get shipment by id %v", err)
	}
	if actual == nil {
		t.Fatalf("shipment not found")
	}

	// it's a dump tmp fix, just the reason is that pq returns timestamp
	// with microsecond precision, so aligning these two
	shipment.CreatedAt = shipment.CreatedAt.Round(time.Microsecond)
	actual.CreatedAt = actual.CreatedAt.UTC()

	if *actual != shipment {
		t.Errorf("expected %#v\n actual %#v", shipment, *actual)
	}
}

func setup(db *sqlx.DB) error {
	_, err := migrate.Exec(db.DB, "postgres", assets.MigrationsSource(), migrate.Up)
	return err
}

func teardown(db *sqlx.DB) error {
	_, err := migrate.Exec(db.DB, "postgres", assets.MigrationsSource(), migrate.Down)
	return err
}
