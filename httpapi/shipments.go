package httpapi

import (
	"encoding/json"
	"net/http"
	"net/url"
	"strconv"

	"github.com/go-chi/chi"
	validation "github.com/go-ozzo/ozzo-validation"
	"github.com/go-ozzo/ozzo-validation/is"
	"github.com/sirupsen/logrus"
	"gitlab.com/vbochko/shipments-svc"
)

type shipmentsHandler struct {
	log *logrus.Entry
	db  shipments.Storage
}

func validateAddShipment(request shipments.Shipment) error {
	return validation.Errors{
		"from.name":         validation.Validate(request.From.Name, validation.Length(0, 30)),
		"to.name":           validation.Validate(request.To.Name, validation.Length(0, 30)),
		"from.email":        validation.Validate(request.From.Email, is.Email),
		"to.email":          validation.Validate(request.To.Email, is.Email),
		"from.address":      validation.Validate(request.From.Address, validation.Length(0, 100)),
		"to.address":        validation.Validate(request.To.Address, validation.Length(0, 100)),
		"from.country_code": validation.Validate(request.From.CountryCode, is.CountryCode2),
		"to.country_code":   validation.Validate(request.To.CountryCode, is.CountryCode2),
		"weight":            validation.Validate(request.WeightKg, validation.Min(0), validation.Max(1000)),
	}.Filter()
}

func (h *shipmentsHandler) addShipment(w http.ResponseWriter, r *http.Request) {
	var shipment shipments.Shipment
	if err := json.NewDecoder(r.Body).Decode(&shipment); err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	if err := validateAddShipment(shipment); err != nil {
		h.log.WithError(err).Debug("invalid add shipment request")
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	price, err := shipments.CalculatePrice(shipment.WeightKg, shipment.From.CountryCode)
	if err != nil {
		h.log.WithError(err).Error("failed to calculate price")
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	shipment.PriceSEK = price

	if err := h.db.Add(&shipment); err != nil {
		h.log.WithError(err).Error("failed to add new shipment to storage")
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusCreated)
	_ = json.NewEncoder(w).Encode(shipment)
}

func (h *shipmentsHandler) getShipment(w http.ResponseWriter, r *http.Request) {
	idStr := chi.URLParam(r, "id")
	id, err := strconv.ParseInt(idStr, 10, 64)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	shipment, err := h.db.Get(id)
	if err != nil {
		h.log.WithError(err).Error("failed to get shipment by id")
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	if shipment == nil {
		w.WriteHeader(http.StatusNotFound)
		return
	}

	_ = json.NewEncoder(w).Encode(shipment)
}

func (h *shipmentsHandler) listShipments(w http.ResponseWriter, r *http.Request) {
	params, err := parseListShipmentsParams(r.URL.Query())
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	// todo: validate params

	shipmentsList, err := h.db.List(params)
	if err != nil {
		h.log.WithError(err).Error("failed to list shipments")
		return
	}

	_ = json.NewEncoder(w).Encode(shipmentsList)
}

func parseListShipmentsParams(params url.Values) (*shipments.ListShipmentsParams, error) {
	var (
		page  uint64 = 0
		limit uint64 = 10
		err   error
	)
	if p := params.Get("page"); p != "" {
		page, err = strconv.ParseUint(p, 10, 64)
		if err != nil {
			return nil, err
		}
	}
	if l := params.Get("limit"); l != "" {
		limit, err = strconv.ParseUint(l, 10, 64)
		if err != nil {
			return nil, err
		}
	}

	return &shipments.ListShipmentsParams{
		Page:  page,
		Limit: limit,
	}, nil
}
