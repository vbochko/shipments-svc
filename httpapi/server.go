package httpapi

import (
	"fmt"
	"net/http"

	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	"github.com/sirupsen/logrus"
	"gitlab.com/vbochko/shipments-svc"
)

// Server.
type Server struct {
	log    *logrus.Entry
	router *chi.Mux
}

// New Server.
func New(log *logrus.Entry, storage shipments.Storage) *Server {
	r := chi.NewRouter()

	r.Use(
		middleware.Recoverer,
		middleware.RequestLogger(&middleware.DefaultLogFormatter{
			Logger: log,
		}),
		middleware.SetHeader("Content-Type", "application/json"),
	)
	r.Route("/shipments", func(r chi.Router) {
		h := &shipmentsHandler{log: log, db: storage}
		r.Post("/", h.addShipment)
		r.Get("/", h.listShipments)
		r.Get("/{id}", h.getShipment)
	})

	return &Server{
		log:    log,
		router: r,
	}
}

// Listen on port.
func (srv *Server) Listen(port int) error {
	addr := fmt.Sprintf(":%d", port)
	srv.log.WithField("addr", addr).Info("up and running")
	return http.ListenAndServe(addr, srv.router)
}
