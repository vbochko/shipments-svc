FROM golang:1.17-alpine

COPY ./ /shipments-svc
WORKDIR /shipments-svc
RUN go build ./cmd/shipments-svc

ENTRYPOINT [ "./shipments-svc" ]