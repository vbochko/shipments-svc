package shipments

import "time"

// Contact info.
type Contact struct {
	ID int64 `db:"id" json:"id"`

	Name        string `db:"name" json:"name"`
	Email       string `db:"email" json:"email"`
	Address     string `db:"address" json:"address"`
	CountryCode string `db:"country_code" json:"country_code"`
}

// Shipment.
type Shipment struct {
	ID int64 `db:"id" json:"id"`

	From Contact `json:"from"`
	To   Contact `json:"to"`

	WeightKg  int64     `db:"weight" json:"weight_kg"`
	PriceSEK  int64     `db:"price" json:"price_sek"`
	CreatedAt time.Time `db:"created_at" json:"created_at"`
}

// Storage should be used to keep/retrieve
// history of shipments.
type Storage interface {
	// Add new Shipment s, generated id is stored in s.
	Add(s *Shipment) error
	Get(id int64) (*Shipment, error)
	List(params *ListShipmentsParams) ([]Shipment, error)
}

// ListShipmentsParams - pagination params.
type ListShipmentsParams struct {
	Page  uint64
	Limit uint64
}
