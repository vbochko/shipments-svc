package shipments

import "fmt"

// CalculatePrice of shipment fromCountry with weightKg.
// Returns price in SEK.
func CalculatePrice(weightKg int64, fromCountry string) (int64, error) {
	price, err := applyWeightRule(weightKg)
	if err != nil {
		return 0, err
	}

	return applyRegionRule(fromCountry, price), nil
}

func applyWeightRule(weightKg int64) (int64, error) {
	switch true {
	case weightKg > 0 && weightKg <= 10:
		return 100, nil
	case weightKg > 10 && weightKg <= 25:
		return 300, nil
	case weightKg > 25 && weightKg <= 50:
		return 500, nil
	case weightKg > 50 && weightKg <= 1000:
		return 2000, nil
	default:
		return 0, fmt.Errorf("weight of %d kg is out of supported range", weightKg)
	}
}

func applyRegionRule(fromCountry string, price int64) int64 {
	switch fromCountry {
	// Nordic region
	case "SE", "NO", "FI", "DK":
		return price
	// EU exclude Nordic
	case "AT", "BE", "BG", "CY", "CZ", "EE", "FR", "DE", "GR",
		"HU", "HR", "IE", "IT", "LV", "LT", "LU", "MT", "NL",
		"PL", "PT", "RO", "SK", "SI", "ES":
		return 3 * price / 2
	default:
		return 5 * price / 2
	}
}
