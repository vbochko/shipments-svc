-- +migrate Up
CREATE TABLE contacts
(
    id BIGSERIAL PRIMARY KEY,
    name TEXT NOT NULL,
    email TEXT NOT NULL,
    address TEXT NOT NULL,
    country_code TEXT NOT NULL
);

CREATE TABLE shipments
(
    id BIGSERIAL PRIMARY KEY,
    from_id BIGINT REFERENCES contacts(id),
    to_id BIGINT REFERENCES contacts(id),
    weight BIGINT NOT NULL,
    price BIGINT NOT NULL,
    created_at TIMESTAMP NOT NULL
);

-- +migrate Down
DROP TABLE shipments;
DROP TABLE contacts;