# shipments-svc

## local deployment:
`$ docker-compose up -d`

(docker-compose version: 1.29.1, docker version: 20.10.12)


This would start server at `http://localhost:8080`

## api
### add a shipment
Request: **POST** `/shipments`
```json
{
    "from": {
        "name": "Tester Sender",
        "email": "tester-sender@gmail.com",
        "address": "1 Sumska Str, Kharkiv",
        "country_code": "AT"
    },
    "to": {
        "name": "Tester Receiver",
        "email": "tester-receiver@gmail.com",
        "address": "1 Derybasivska Str, Odesa",
        "country_code": "UA"
    },
    "weight_kg": 15
}
```

Response:

```json
{
    "id": 1,
    "from": {
        "id": 1,
        "name": "Tester Sender",
        "email": "tester-sender@gmail.com",
        "address": "1 Sumska Str, Kharkiv",
        "country_code": "AT"
    },
    "to": {
        "id": 2,
        "name": "Tester Receiver",
        "email": "tester-receiver@gmail.com",
        "address": "1 Derybasivska Str, Odesa",
        "country_code": "UA"
    },
    "weight_kg": 15,
    "price_sek": 450,
    "created_at": "2022-02-08T16:15:57.996855926Z"
}
```

### get the shipment by id
Request: **GET** /shipments/1

Response:
```json
{
    "id": 1,
    "from": {
        "id": 1,
        "name": "Tester Sender",
        "email": "tester-sender@gmail.com",
        "address": "1 Sumska Str, Kharkiv",
        "country_code": "AT"
    },
    "to": {
        "id": 2,
        "name": "Tester Receiver",
        "email": "tester-receiver@gmail.com",
        "address": "1 Derybasivska Str, Odesa",
        "country_code": "UA"
    },
    "weight_kg": 15,
    "price_sek": 450,
    "created_at": "2022-02-08T16:15:57.996855926Z"
}
```


### list shipments
Request: **GET** /shipments?page=1&limit=2

Response:
```json
[
    {
        "id": 3,
        "from": {
            "id": 5,
            "name": "Tester Sender",
            "email": "tester-sender@gmail.com",
            "address": "1 Sumska Str, Kharkiv",
            "country_code": "NO"
        },
        "to": {
            "id": 6,
            "name": "Tester Receiver",
            "email": "tester-receiver@gmail.com",
            "address": "1 Derybasivska Str, Odesa",
            "country_code": "UA"
        },
        "weight_kg": 15,
        "price_sek": 300,
        "created_at": "2022-02-08T16:18:48.974024Z"
    },
    {
        "id": 4,
        "from": {
            "id": 7,
            "name": "Tester Sender",
            "email": "tester-sender@gmail.com",
            "address": "1 Sumska Str, Kharkiv",
            "country_code": "US"
        },
        "to": {
            "id": 8,
            "name": "Tester Receiver",
            "email": "tester-receiver@gmail.com",
            "address": "1 Derybasivska Str, Odesa",
            "country_code": "UA"
        },
        "weight_kg": 15,
        "price_sek": 750,
        "created_at": "2022-02-08T16:19:05.480528Z"
    }
]
```