package main

import (
	"database/sql"
	"fmt"
	"os"

	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
	migrate "github.com/rubenv/sql-migrate"
	"github.com/sirupsen/logrus"
	"github.com/urfave/cli/v2"
	"gitlab.com/vbochko/shipments-svc/assets"
	"gitlab.com/vbochko/shipments-svc/httpapi"
	"gitlab.com/vbochko/shipments-svc/postgres"
)

func main() {
	app := &cli.App{
		Name:  "shipments-svc",
		Usage: "shipments server",
		Flags: []cli.Flag{
			&cli.StringFlag{Name: "db", Usage: "db url", Required: true},
		},
		Commands: []*cli.Command{
			{
				Name: "migrate",
				Subcommands: []*cli.Command{
					{
						Name:   "up",
						Usage:  "apply new migrations",
						Action: migrateDB(migrate.Up),
					},
					{
						Name:   "down",
						Usage:  "rollback migrations",
						Action: migrateDB(migrate.Down),
					},
				},
			},
			{
				Name:  "run",
				Usage: "run server",
				Flags: []cli.Flag{
					&cli.IntFlag{Name: "port", Value: 8080, Usage: "http api port"},
				},
				Action: runApp,
			},
		},
	}

	if err := app.Run(os.Args); err != nil {
		logrus.WithError(err).Error("app failed")
	}
}

func runApp(c *cli.Context) error {
	dsn := c.String("db")
	db, err := sqlx.Connect("postgres", dsn)
	if err != nil {
		return cli.Exit(fmt.Errorf("failed to connect to db at %s: %w", dsn, err), 1)
	}

	log := logrus.New()
	log.SetLevel(logrus.DebugLevel)
	srv := httpapi.New(log.WithField("svc", "api"), postgres.New(db))

	if srv.Listen(c.Int("port")); err != nil {
		return cli.Exit(fmt.Errorf("failed to start server: %w", err), 1)
	}

	return nil
}

func migrateDB(dir migrate.MigrationDirection) func(*cli.Context) error {
	return func(c *cli.Context) error {
		dsn := c.String("db")
		db, err := sql.Open("postgres", dsn)
		if err != nil {
			return fmt.Errorf("failed to open db at %s: %w", dsn, err)
		}

		n, err := migrate.Exec(db, "postgres", assets.MigrationsSource(), dir)
		if err != nil {
			return fmt.Errorf("failed to run migrations: %w", err)
		}

		logrus.Infof("run %d migrations", n)

		return nil
	}
}
